import React from 'react'
import Title from './title'
import Showtodo from './showtodo'

import '../Stylesheet/style-all.css'

class Form extends React.Component {

  state = {
    todolist:[],
    texttodo:""
  };

  onChangeinput = (event) =>{
    this.setState({texttodo : event.target.value});
  }

  onSubmitform = (event) =>{
    event.preventDefault();
    console.log(this.state.texttodo);
    this.setState({todolist:[...this.state.todolist , this.state.texttodo] , texttodo:""});
  }

  removeItem = (index) => {
    this.setState({
      todolist: this.state.todolist.filter((item , indexEl)=> indexEl!==index)
    })
  }

  render(){
    return(
        <div className='div-form-parent'>
          <form onSubmit={this.onSubmitform}>
            <Title />
            <input id="idinput" value={this.state.texttodo} onChange={this.onChangeinput} className='input-add-text' type='text'/>
            <Showtodo
              showtodo={this.state.todolist}
              removetodo={this.removeItem}
            />
          </form>

        </div>
    )
  }
}

export default Form
