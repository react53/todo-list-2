import React from 'react';

function onClickremove (props, index) {
  console.log("On click remove called");
  props.removetodo(index);
}

function testFunc () {
  console.log("TEST @");
}

const Showtodo = props =>{
  return(
    <div>
      <ul className="ul-item">
        {props.showtodo.map((item , index)=>(
          <li className='li-item' key={index}>
            <span onClick={onClickremove.bind(this, props, index)} className="span-remove-item">
              <i className="delete icon"></i>
            </span>
            {item}
          </li>
        ))}
      </ul>
    </div>
  )
}


export default Showtodo
