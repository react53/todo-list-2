import React from 'react';
import ReactDOM from 'react-dom';

import App from './Component/App';

const html = document.getElementById('root');


ReactDOM.render(<App />, html)
